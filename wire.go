//go:build wireinject
// +build wireinject

package main

import (
	"github.com/google/wire"
	"interview_test_upload/api"
	"interview_test_upload/internal/conf"
	"interview_test_upload/internal/domain"
	"interview_test_upload/internal/infra"
	"net/http"
)

func server() (*http.Server, error) {
	panic(wire.Build(conf.LoadConf, domain.ProvideSet, infra.ProvideSet, api.ProvideSet))
}
