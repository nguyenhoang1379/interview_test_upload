package main

// @title Upload Service
// @version 1.0
// @description This is Upload Service API.

// @contact.name Nguyen Hai Hoang
// @contact.email nguyenhoang1379@gmail.com
// @BasePath /

// @securityDefinitions.apikey JwtToken
// @in header
// @name Authorization
func main() {
	srv, err := server()
	if err != nil {
		panic(err)
	}
	err = srv.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

//func rootCmd() *cobra.Command {
//	return nil
//}
