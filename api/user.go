package api

import (
	"github.com/gin-gonic/gin"
	"interview_test_upload/internal/domain"
	"net/http"
)

// CreateUser
// @Summary CreateUser
// @Description CreateUser
// @Tags user
// @Accept json
// @Produce json
// @Param body body domain.RegisterUserInput true "JSON body"
// @Success 200 {object} domain.User
// @Router /v1/users [post]
func (api *HttpApi) CreateUser(c *gin.Context) {
	var input domain.RegisterUserInput
	err := c.BindJSON(&input)
	if err != nil {
		c.Error(err)
		return
	}
	user, err := api.useCase.User.RegisterUser(c, input)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, user)
}

// ChangePassword
// @Summary ChangePassword
// @Description ChangePassword
// @Security JwtToken
// @Tags user
// @Accept json
// @Produce json
// @Param body body domain.ChangeUserInput true "JSON body"
// @Success 200 {object} interface{}
// @Router /v1/users/change-password [put]
func (api *HttpApi) ChangePassword(c *gin.Context) {
	var input domain.ChangeUserInput
	err := c.BindJSON(&input)
	if err != nil {
		c.Error(err)
		return
	}
	err = api.useCase.User.ChangePassword(c, input)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}
