package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// UploadObject
// @Summary	Upload object
// @Description	Upload object
// @Tags	Storage
// @Security JwtToken
// @Accept	multipart/form-data
// @Security ApiKeyAuth
// @Produce	json
// @Param	file	  formData file   true "Upload file"
// @Router /v1/storage/upload [post]
func (api *HttpApi) UploadObject(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		c.Error(err)
		return
	}
	object, err := api.useCase.Storage.PutObject(c, file)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, object)
}

// DownloadObject
// @Summary DownloadObject
// @Description DownloadObject
// @Security JwtToken
// @Tags Storage
// @Accept json
// @Produce json
// @Param key query string true "key"
// @Success 304 {object} interface{}
// @Router /v1/storage/download [get]
func (api *HttpApi) DownloadObject(c *gin.Context) {
	key := c.Query("key")
	url, err := api.useCase.Storage.GetObjectUrl(c, key)
	if err != nil {
		c.Error(err)
		return
	}
	c.Redirect(304, url)
	//http.Redirect(c.Writer, c.Request, url, http.StatusNotModified)
}

// ListObject
// @Summary ListObject
// @Description ListObject
// @Security JwtToken
// @Tags Storage
// @Accept json
// @Produce json
// @Success 200 {array} domain.Object
// @Router /v1/storage [get]
func (api *HttpApi) ListObject(c *gin.Context) {
	objects, err := api.useCase.Storage.ListObjects(c)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, objects)
}
