package api

import (
	"github.com/gin-gonic/gin"
	"interview_test_upload/internal/domain"
	"interview_test_upload/internal/pkgs/apperr"
	"net/http"
	"strings"
)

const Bearer = "Bearer "

// Login
// @Summary Login
// @Description Login
// @Tags auth
// @Accept json
// @Produce json
// @Param body body domain.LoginInput true "JSON body"
// @Success 200 {object} domain.UserCredential
// @Router /v1/auth/login [post]
func (api *HttpApi) Login(c *gin.Context) {
	var input domain.LoginInput
	err := c.BindJSON(&input)
	if err != nil {
		c.Error(err)
		return
	}
	cred, err := api.useCase.Auth.Login(c, input)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, cred)
}

func (api *HttpApi) AuthFilter(c *gin.Context) {
	authorization := c.GetHeader("Authorization")
	auths := strings.SplitN(authorization, " ", 2)
	if len(auths) != 2 || !strings.EqualFold(auths[0], "Bearer") {
		c.Error(apperr.InvalidUserCredential)
		return
	}
	jwtToken := auths[1]
	credential, err := api.useCase.Auth.ValidateToken(c, jwtToken)
	if err != nil {
		c.Error(apperr.InvalidUserCredential)
		return
	}
	c.Request = c.Request.WithContext(domain.ContextWithUserCredential(c.Request.Context(), credential))
	c.Next()
}
