package api

import (
	"github.com/gin-gonic/gin"
	"github.com/google/wire"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "interview_test_upload/docs"
	"interview_test_upload/internal/conf"
	"interview_test_upload/internal/domain"
	"net/http"
)

var ProvideSet = wire.NewSet(HttpServer)

type HttpApi struct {
	useCase *domain.UseCase
}

func (api *HttpApi) setup(router *gin.Engine) {
	router.Use(CORSMiddleware(), func(c *gin.Context) {
		c.Next()
		err := c.Errors.Last()
		if err != nil {
			c.JSON(http.StatusInternalServerError, err.JSON())
			c.Abort()
		}
	})
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	router.POST("/v1/auth/login", api.Login)
	router.POST("/v1/users", api.CreateUser)
	router.PUT("/v1/users/change-password", api.AuthFilter, api.ChangePassword)
	router.POST("/v1/storage.upload", api.AuthFilter, api.UploadObject)
	router.GET("/v1/storage/*key", api.AuthFilter, api.DownloadObject)
	router.GET("/v1/storage/download", api.AuthFilter, api.ListObject)
}

func HttpServer(cfg *conf.Conf, useCase *domain.UseCase) (*http.Server, error) {
	var server = new(http.Server)
	var router = gin.Default()
	router.ContextWithFallback = true
	server.Handler = router
	server.Addr = cfg.Server.Addr
	httpApi := &HttpApi{useCase: useCase}
	httpApi.setup(router)
	return server, nil
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		//c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Max")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	}
}
