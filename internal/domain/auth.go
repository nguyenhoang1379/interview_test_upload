package domain

import (
	"context"
	"golang.org/x/crypto/bcrypt"
)

type AuthUseCase struct {
	authRepo AuthRepo
	userRepo UserRepo
}

type AuthRepo interface {
	GenerateCredential(ctx context.Context, user User) (UserCredential, error)
	ParseToken(ctx context.Context, token string) (UserCredential, error)
}

func NewAuth(repo AuthRepo, userRepo UserRepo) *AuthUseCase {
	return &AuthUseCase{authRepo: repo, userRepo: userRepo}
}

func (us *AuthUseCase) Login(ctx context.Context, in LoginInput) (credential UserCredential, err error) {
	if err = in.Validate(); err != nil {
		return UserCredential{}, err
	}
	user, err := us.userRepo.GetUserByUsername(ctx, in.Username)
	if err != nil {
		return UserCredential{}, err
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(in.Password))
	if err != nil {
		return UserCredential{}, err
	}
	return us.authRepo.GenerateCredential(ctx, user)
}

func (us *AuthUseCase) ValidateToken(ctx context.Context, token string) (UserCredential, error) {
	return us.authRepo.ParseToken(ctx, token)
}
