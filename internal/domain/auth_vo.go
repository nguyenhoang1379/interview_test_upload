package domain

import "time"

type LoginInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (r LoginInput) Validate() error {
	return nil
}

type LoginOutput struct {
	AccessToken string    `json:"accessToken"`
	ExpiresAt   time.Time `json:"expiresAt"`
}
