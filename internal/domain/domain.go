package domain

import "github.com/google/wire"

var ProvideSet = wire.NewSet(NewAuth, NewUserUseCase, NewStorageUseCase, wire.Struct(new(UseCase), "*"))

type UseCase struct {
	Auth    *AuthUseCase
	User    *UserUseCase
	Storage *StorageUseCase
}
