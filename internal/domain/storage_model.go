package domain

import "time"

type Object struct {
	ETag         *string    `json:"eTag"`
	Key          *string    `json:"key"`
	LastModified *time.Time `json:"lastModified"`
	Size         int64      `json:"size"`
}
