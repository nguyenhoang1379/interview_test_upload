package domain

import (
	"context"
	"time"
)

type UserCredential struct {
	Username    string    `json:"username"`
	ExpiresAt   time.Time `json:"expires_at"`
	AccessToken string    `json:"access_token"`
}

type userCredentialKey struct{}

func ContextWithUserCredential(ctx context.Context, credential UserCredential) context.Context {
	return context.WithValue(ctx, userCredentialKey{}, credential)
}

func UserCredentialFromContext(ctx context.Context) (UserCredential, bool) {
	value := ctx.Value(userCredentialKey{})
	if value == nil {
		return UserCredential{}, false
	}
	return value.(UserCredential), true
}
