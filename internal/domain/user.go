package domain

import (
	"context"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"interview_test_upload/internal/pkgs/apperr"
	"time"
)

type UserUseCase struct {
	userRepo UserRepo
}

type UserRepo interface {
	CreateUser(ctx context.Context, user *User) error
	UpdateUser(ctx context.Context, user *User) error
	GetUserByUsername(ctx context.Context, username string) (User, error)
}

func NewUserUseCase(repo UserRepo) *UserUseCase {
	return &UserUseCase{userRepo: repo}
}

func (us *UserUseCase) RegisterUser(ctx context.Context, in RegisterUserInput) (user User, err error) {
	if err = in.Validate(); err != nil {
		return User{}, err
	}
	if _, err = us.userRepo.GetUserByUsername(ctx, in.Username); !errors.Is(err, apperr.RecNotFound) {
		if err != nil {
			return User{}, err
		} else {
			return User{}, apperr.UserAlready
		}
	}
	hashPwd, err := bcrypt.GenerateFromPassword([]byte(in.Password), bcrypt.DefaultCost)
	if err != nil {
		return User{}, err
	}
	user = User{
		Username:  in.Username,
		Password:  string(hashPwd),
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	if err = us.userRepo.CreateUser(ctx, &user); err != nil {
		return User{}, err
	}
	return user, nil
}

func (us *UserUseCase) ChangePassword(ctx context.Context, in ChangeUserInput) (err error) {
	if err = in.Validate(); err != nil {
		return err
	}
	credential, ok := UserCredentialFromContext(ctx)
	if !ok {
		return apperr.InvalidUserCredential
	}
	user, err := us.userRepo.GetUserByUsername(ctx, credential.Username)
	if err != nil {
		return err
	}
	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(in.OldPassword)); err != nil {
		return err
	}
	hashPwd, err := bcrypt.GenerateFromPassword([]byte(in.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.Password = string(hashPwd)
	user.UpdatedAt = time.Now()
	return us.userRepo.UpdateUser(ctx, &user)
}
