package domain

import (
	"context"
	"interview_test_upload/internal/pkgs/apperr"
	"mime/multipart"
	"strings"
)

type StorageRepo interface {
	PutObject(ctx context.Context, username string, file *multipart.FileHeader) (*Object, error)
	GetObjectUrl(ctx context.Context, key string) (string, error)
	ListObjects(ctx context.Context, username string) ([]Object, error)
}

type StorageUseCase struct {
	repo StorageRepo
}

func (s StorageUseCase) PutObject(ctx context.Context, file *multipart.FileHeader) (*Object, error) {

	userCredential, ok := UserCredentialFromContext(ctx)
	if !ok {
		return nil, apperr.InvalidUserCredential
	}
	return s.repo.PutObject(ctx, userCredential.Username, file)
}

func (s StorageUseCase) GetObjectUrl(ctx context.Context, key string) (string, error) {
	userCredential, ok := UserCredentialFromContext(ctx)
	if !ok || !strings.HasPrefix(key, userCredential.Username) {
		return "", apperr.InvalidUserCredential
	}

	return s.repo.GetObjectUrl(ctx, key)
}

func (s StorageUseCase) ListObjects(ctx context.Context) ([]Object, error) {
	userCredential, ok := UserCredentialFromContext(ctx)
	if !ok {
		return nil, apperr.InvalidUserCredential
	}
	return s.repo.ListObjects(ctx, userCredential.Username)
}

func NewStorageUseCase(repo StorageRepo) *StorageUseCase {
	return &StorageUseCase{repo: repo}
}
