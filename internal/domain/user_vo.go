package domain

type RegisterUserInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (r RegisterUserInput) Validate() error {
	return nil
}

type ChangeUserInput struct {
	OldPassword string `json:"oldPassword"`
	NewPassword string `json:"newPassword"`
}

func (r ChangeUserInput) Validate() error {
	return nil
}
