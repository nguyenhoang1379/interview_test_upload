package conf

import (
	"github.com/spf13/viper"
	"time"
)

func LoadConf() (*Conf, error) {
	var cof Conf
	viper.SetConfigFile("config.yaml")
	viper.SetConfigType("yaml")
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	err = viper.Unmarshal(&cof)
	if err != nil {
		return nil, err
	}
	return &cof, nil
}

type Conf struct {
	Infra  *Infra
	Server *Server
}

type Server struct {
	Addr string
}

type Infra struct {
	Storage struct {
		Region        string
		AccessKey     string
		SecretKey     string
		Bucket        string
		ObjectMaxSize int
	}
	Database struct {
		Dns         string
		MaxOpenCons int
		MaxIdleCons int
	}
	Jwt struct {
		Issuer              string
		SecretKey           string
		AccessTokenLifeTime time.Duration
	}
}
