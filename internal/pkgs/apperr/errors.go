package apperr

import "errors"

var (
	RecNotFound           = errors.New("rec not found")
	UserAlready           = errors.New("user already exists")
	InvalidUserCredential = errors.New("invalid user credential")
)
