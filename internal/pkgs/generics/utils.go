package generics

func ToPtr[T any](x T) *T {
	return &x
}

func FromPtr[T any](x *T) (value T) {
	if x == nil {
		return value
	}
	return *x
}

func MapArray[S any, D any](src []S, f func(S) D) []D {
	var dest = make([]D, len(src))
	for i := range src {
		dest[i] = f(src[i])
	}
	return dest
}
