package infra

import (
	"context"
	"interview_test_upload/internal/domain"
	"time"
)

type userRepo struct {
	*Infra
}

func UserRepo(infra *Infra) (domain.UserRepo, error) {
	_, err := infra.db.Exec(`
CREATE TABLE IF NOT EXISTS  users (
	username  VARCHAR(255) PRIMARY KEY,
	password   VARCHAR(250) DEFAULT NULL,
	created_at DATETIME,
	updated_at DATETIME
)`)
	if err != nil {
		return nil, err
	}
	return &userRepo{infra}, nil
}

func (r *userRepo) CreateUser(ctx context.Context, user *domain.User) error {
	user.CreatedAt = time.Now()
	user.UpdatedAt = user.CreatedAt
	_, err := r.db.NamedExecContext(ctx, `
INSERT INTO users (username,password,created_at,updated_at)
VALUES (:username,:password,:created_at,:updated_at)
`, user)
	return err
}

func (r *userRepo) UpdateUser(ctx context.Context, user *domain.User) error {
	user.UpdatedAt = time.Now()
	_, err := r.db.NamedExecContext(ctx, `
UPDATE users 
SET password = :password,
	updated_at = :updated_at   
WHERE username = :username
`, user)
	return err
}

func (r *userRepo) GetUserByUsername(ctx context.Context, username string) (user domain.User, err error) {
	err = r.db.QueryRowxContext(ctx, `
SELECT username, password, created_at, updated_at
FROM users
WHERE username = $1
`, username).StructScan(&user)
	return user, err
}
