package infra

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
	"interview_test_upload/internal/domain"
	"interview_test_upload/internal/pkgs/generics"
	"mime/multipart"
	"time"
)

type storageRepo struct {
	*Infra
}

func StorageRepo(infra *Infra) domain.StorageRepo {
	return &storageRepo{infra}
}

func (s storageRepo) PutObject(ctx context.Context, username string, file *multipart.FileHeader) (*domain.Object, error) {
	if file.Size > int64(s.cfg.Storage.ObjectMaxSize*1024) {
		return nil, fmt.Errorf("max file size %dkb", s.cfg.Storage.ObjectMaxSize)
	}
	f, err := file.Open()
	if err != nil {
		return nil, err
	}
	input := &s3.PutObjectInput{
		Bucket: &s.cfg.Storage.Bucket,
		Key:    generics.ToPtr(fmt.Sprintf("%s/%s", username, file.Filename)),
		Body:   f,
	}
	if contentType := file.Header.Get("Content-Type"); contentType != "" {
		input.ContentType = generics.ToPtr(contentType)
	}
	if contentDisposition := file.Header.Get("Content-Disposition"); contentDisposition != "" {
		input.ContentDisposition = generics.ToPtr(contentDisposition)
	}
	object, err := s.s3Client.PutObject(ctx, input)
	if err != nil {
		return nil, err
	}
	return &domain.Object{
		ETag:         object.ETag,
		Key:          input.Key,
		LastModified: generics.ToPtr(time.Now()),
		Size:         file.Size,
	}, nil
}

func (s storageRepo) GetObjectUrl(ctx context.Context, key string) (string, error) {
	input := &s3.GetObjectInput{
		Bucket:          generics.ToPtr(s.cfg.Storage.Bucket),
		Key:             generics.ToPtr(key),
		ResponseExpires: generics.ToPtr(time.Now().Add(time.Minute * 5)),
	}
	presignGetObject, err := s.s3PresignClient.PresignGetObject(ctx, input)
	if err != nil {
		return "", err
	}
	return presignGetObject.URL, nil
}

func (s storageRepo) ListObjects(ctx context.Context, username string) ([]domain.Object, error) {
	input := &s3.ListObjectsInput{
		Bucket: generics.ToPtr(s.cfg.Storage.Bucket),
		Prefix: generics.ToPtr(username),
	}
	objects, err := s.s3Client.ListObjects(ctx, input)
	if err != nil {
		return nil, err
	}
	return generics.MapArray(objects.Contents, func(s types.Object) domain.Object {
		return domain.Object{
			ETag:         s.ETag,
			Key:          s.Key,
			LastModified: s.LastModified,
			Size:         s.Size,
		}
	}), nil
}
