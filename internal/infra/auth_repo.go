package infra

import (
	"context"
	"errors"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
	"interview_test_upload/internal/domain"
	"time"
)

type authRepo struct {
	*Infra
}

func (r *authRepo) GenerateCredential(_ context.Context, user domain.User) (domain.UserCredential, error) {
	now := time.Now()
	exp := now.Add(r.cfg.Jwt.AccessTokenLifeTime)
	claims := jwt.RegisteredClaims{
		Issuer:    r.cfg.Jwt.Issuer,
		Subject:   user.Username,
		Audience:  []string{r.cfg.Jwt.Issuer},
		ExpiresAt: jwt.NewNumericDate(exp),
		NotBefore: jwt.NewNumericDate(now),
		IssuedAt:  jwt.NewNumericDate(now),
		ID:        uuid.New().String(),
	}
	tokenStr, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(r.cfg.Jwt.SecretKey))
	if err != nil {
		return domain.UserCredential{}, err
	}
	return domain.UserCredential{
		Username:    user.Username,
		ExpiresAt:   claims.ExpiresAt.Time,
		AccessToken: tokenStr,
	}, nil
}

func (r *authRepo) ParseToken(_ context.Context, tokeStr string) (domain.UserCredential, error) {
	token, err := jwt.ParseWithClaims(tokeStr, &jwt.RegisteredClaims{}, func(token *jwt.Token) (interface{}, error) {
		if token.Method != jwt.SigningMethodHS256 {
			return nil, errors.New("invalid token")
		}
		return []byte(r.cfg.Jwt.SecretKey), nil
	})
	if err != nil {
		return domain.UserCredential{}, err
	}
	claims, ok := token.Claims.(*jwt.RegisteredClaims)
	if !ok {
		return domain.UserCredential{}, errors.New("invalid token")
	}
	return domain.UserCredential{
		Username:    claims.Subject,
		ExpiresAt:   claims.ExpiresAt.Time,
		AccessToken: tokeStr,
	}, nil
}

func AuthRepo(infra *Infra) domain.AuthRepo {
	return &authRepo{infra}
}
