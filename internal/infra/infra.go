package infra

import (
	"context"
	"database/sql"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	_ "github.com/glebarez/go-sqlite"
	"github.com/google/wire"
	"github.com/jmoiron/sqlx"
	"interview_test_upload/internal/conf"
	"interview_test_upload/internal/pkgs/apperr"
	"regexp"
	"strings"
)

var ProvideSet = wire.NewSet(NewInfra, AuthRepo, UserRepo, StorageRepo)

type Infra struct {
	cfg             *conf.Infra
	s3Client        *s3.Client
	s3PresignClient *s3.PresignClient
	db              *sqlx.DB
}

func NewInfra(cfg *conf.Conf) (*Infra, error) {
	var infra = &Infra{cfg: cfg.Infra}
	awsCfg, err := config.LoadDefaultConfig(context.Background(),
		config.WithRegion(infra.cfg.Storage.Region),
		config.WithCredentialsProvider(credentials.NewStaticCredentialsProvider(infra.cfg.Storage.AccessKey, infra.cfg.Storage.SecretKey, "")),
	)
	if err != nil {
		return nil, err
	}
	infra.s3Client = s3.NewFromConfig(awsCfg)
	infra.s3PresignClient = s3.NewPresignClient(infra.s3Client)
	infra.db, err = sqlx.Open("sqlite", infra.cfg.Database.Dns)
	if err != nil {
		return nil, err
	}
	infra.db.SetMaxOpenConns(infra.cfg.Database.MaxOpenCons)
	infra.db.SetMaxIdleConns(infra.cfg.Database.MaxIdleCons)
	if err = infra.db.Ping(); err != nil {
		return nil, err
	}
	sqlx.NameMapper = func(str string) string {
		snake := regexp.MustCompile("(.)([A-Z][a-z]+)").ReplaceAllString(str, "${1}_${2}")
		snake = regexp.MustCompile("([a-z0-9])([A-Z])").ReplaceAllString(snake, "${1}_${2}")
		return strings.ToLower(snake)
	}
	sql.ErrNoRows = apperr.RecNotFound
	return infra, nil
}
