# Upload Service

## Tính năng

* User
  * tạo tài khoản
  * đổi mật khẩu
* Auth
  * Login
* Storage
    * lấy danh sách objects
    * upload object
    * download object

Link Swagger: http://127.0.0.1:8000/swagger/index.html

## Kiến trúc
Mô hình áp dụng: domain driven design

Layout:

* **api:** _tầng ứng dụng chứa các http api handler_
* 
* **docs:** _tài liệu api_
* 
* **internal**
  * **conf:** _cấu hình_
  * 
  * domain: tầng nghiệp vụ
    * **useCase:** _thể hiện tất cả các functional của hệ thống_ 
    * **model:** _domain model (đối tượng nghiệp vụ)_
    * **vo:** _value object, là các cấu trúc dùng để làm input, output của các nghiệp vụ_
    * **repo**: _repository là các interface cung cấp data cho useCase_
  *
  * **infra:** hạ tầng 
    * _chứa các infrastructure của hệ thống (datasource)_
    * _impl các interface Repo của tầng domain_
  *
  * **pkgs:** _chứa những gói tiện ích dùng chung của hệ thống_

Mô tả:

> tầng nghiệp chứa và thể hiện toàn bộ nghiệp vụ của hệ thống,
> để đảm bảo tính linh hoạt, lấy nghiệp vụ làm trung tâm,
> tầng này không được phụ thuộc vào tầng ứng dụng hay hạ tầng.
> Mà ngược la hạ tầng và ứng dụng phải phụ thuộc nghệp vụ.\
> Khi mở rộng hệ thống, có thể chia tầng domain thành từng package nhỏ tương 
> ứng với từng miê nghiệp vụ khác nhau.\
> Các miền nghiệp vụ không nên phụ thuộc vào nhau, mà nên phụ thuộc vào interface.
 
Note: 

> Trong demo hiện tại đang chưa chia các miền nghiệp thành package riêng cũng như
> đang dependency vào nhau vi phạm điều vừa nêu bên trên

## Lib

* Viper: đọc config, config default, theo dõi config, hot reload... 
nhưng trong scope hiện tại(demo) chỉ dùng để đọc config
* Wire: dependency injection(code gen)
* Sqlite: db driver
* Sqlx: trình bổ trợ sql cho golang
* Aws SDK: aws, s3 driver
* Gin: http router
* Swaggo: generate & serve swagger docs